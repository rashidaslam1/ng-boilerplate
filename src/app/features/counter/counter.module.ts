import { CounterRoutingModule } from './counter-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './views/home/home.component';
import { ResultComponent } from './component/result/result.component';
import { StoreModule } from '@ngrx/store';
import { counterReducer } from './state';


@NgModule({
  declarations: [HomeComponent, ResultComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature('counter', counterReducer),
    CounterRoutingModule
  ]
})
export class CounterModule { }
