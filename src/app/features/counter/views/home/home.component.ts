import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { map } from "rxjs/operators";
import { Observable, Subscription } from "rxjs";
import { AppState, userSelector } from 'src/app/state';
import { User } from 'src/app/shared/model/user.interface';
import { countSelector, increment, decrement, add, subtract } from '../../state';
import { Counter } from '../../state/counter.reducer';
import { historySelector } from '../../state/counter.selector';
import { clearHistory } from '../../state/counter.action';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {
  countState$: Observable<number>;
  historyState$: Observable<Array<number>>;
  count: number;
  history: Array<number>;
  constructor(private store: Store<AppState>) {
    this.countState$ = store.pipe(select(countSelector))
    this.historyState$ = store.pipe(select(historySelector))

  }

  ngOnInit() {
    this.countState$
      .subscribe((item) => {
        this.count = item;
      });
    this.historyState$
      .subscribe((item) => {
        this.history = item;
      });
  }
  onIncrement() {
    this.store.dispatch(increment())
  }
  onDecrement() {
    this.store.dispatch(decrement())
  }
  onAdd() {
    this.store.dispatch(add({ payload: 5 }))
  }
  onSubtract() {
    this.store.dispatch(subtract({ payload: 5 }))
  }
  onClearHistory(){
    this.store.dispatch(clearHistory())
  }
}
