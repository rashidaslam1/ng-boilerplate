import { createReducer, on, Action } from "@ngrx/store";
import { increment, decrement, add, subtract, clearHistory } from './counter.action';

export interface Counter {
    value: number,
    history: Array<number>
}
const initialState: Counter = {
    value: 0,
    history: []
}

const reducer = createReducer(
    initialState,
    on(increment, state => ({ ...state, value: state.value + 1, history: [...state.history, state.value + 1] })),
    on(decrement, state => ({ ...state, value: state.value - 1, history: [...state.history, state.value - 1] })),
    on(add, (state, { payload }) => ({ ...state, value: state.value + payload, history: [...state.history, state.value + payload] })),
    on(subtract, (state, { payload }) => ({ ...state, value: state.value - payload, history: [...state.history, state.value - payload] })),
    on(clearHistory, state => ({ ...state, history: [] }))
)

export function counterReducer(state: Counter | undefined, action: Action) {
    return reducer(state, action);
}


