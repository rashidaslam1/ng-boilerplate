
import { createAction, props } from "@ngrx/store";


export const increment = createAction(
    '[Counter] Increment'
)

export const decrement = createAction(
    '[Counter] decrement'
)


export const add = createAction(
    '[Counter] add',
    props<{payload:number}>()
)


export const subtract = createAction(
    '[Counter] subtract',
    props<{payload:number}>()
)
export const clearHistory = createAction(
    '[Counter] clear history'
)

