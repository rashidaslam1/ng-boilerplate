import { createFeatureSelector, createSelector } from "@ngrx/store";
import { Counter } from './counter.reducer';



const getCountState = createFeatureSelector<Counter>('counter');

export const countSelector = createSelector(
    getCountState,
    (state: Counter) => state.value
)
export const historySelector = createSelector(
    getCountState,
    (state: Counter) => state.history
)