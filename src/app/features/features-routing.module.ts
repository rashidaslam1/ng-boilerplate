import { FEATURE_ROUTES } from './features.constant';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FeaturesComponent } from './features.component';


const routes: Routes = [
  {
    path: '',
    component: FeaturesComponent,
    children: [
      {
        path: FEATURE_ROUTES.TODO,
        loadChildren: () => import('./todo/todo.module').then((m) => m.TodoModule)
      },
      {
        path: FEATURE_ROUTES.COUNTER,
        loadChildren: () => import('./counter/counter.module').then((m) => m.CounterModule)
      }
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
