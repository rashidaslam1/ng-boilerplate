import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ToDoState } from './todo.reducer';



const getTodoState = createFeatureSelector<ToDoState>('todo');

export const todoSelector = createSelector(
    getTodoState,
    (state: ToDoState) => state.list
)