import { createReducer, on, Action } from "@ngrx/store";
import { add, remove } from './todo.action';
import { isNgTemplate } from '@angular/compiler';

export interface ToDo {
    id: number;
    item: string;
}

export interface ToDoState {
    list: Array<ToDo>
}
const initialState: ToDoState = {
    list: []
}
const reducer = createReducer(
    initialState,
    on(add, (state, { payload }) => ({ ...state, list: [...state.list, payload] })),
    on(remove, (state, { payload }) => ({ ...state, list: state.list.filter((item: ToDo) => item.id !== payload) }))
)

export function todoReducer(state: ToDoState | undefined, action: Action) {
    return reducer(state, action)
}