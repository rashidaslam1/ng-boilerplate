import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ToDoState, ToDo, todoSelector } from '../../state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  list: Observable<ToDo>;
  constructor(private store: Store<ToDoState>) {
  }

  ngOnInit() {
    this.store.select(todoSelector).subscribe((item) => {
      debugger
    })
  }
}
