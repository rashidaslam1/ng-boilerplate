import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoRoutingModule } from './todo-routing.module';
import { HomeComponent } from './views/home/home.component';
import { CreateComponent } from './views/create/create.component';
import { ListComponent } from './views/list/list.component';
import { StoreModule } from '@ngrx/store';
import { todoReducer } from './state';


@NgModule({
  declarations: [HomeComponent, CreateComponent, ListComponent],
  imports: [
    CommonModule,
    TodoRoutingModule,
    StoreModule.forFeature('todo', todoReducer)
  ]
})

export class TodoModule { }
