import { TODO_ROUTES } from './todo.constant';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateComponent } from './views/create/create.component';
import { HomeComponent } from './views/home/home.component';
import { ListComponent } from './views/list/list.component';


const routes: Routes = [
  {
    path: '', pathMatch: 'full', redirectTo: TODO_ROUTES.HOME
  },
  {
    path: TODO_ROUTES.HOME, component: HomeComponent
  },
  {
    path: TODO_ROUTES.CREATE, component: CreateComponent
  },
  {
    path: TODO_ROUTES.LIST, component: ListComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodoRoutingModule { }
