export interface User {
    firstName: string;
    lastName: string;
    isAuthorized: boolean;
}