import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GLOBAL_ROUTES } from './app.constant';


const routes: Routes = [
  {
    path: '', pathMatch: 'full', redirectTo: GLOBAL_ROUTES.HOME
  },
  {
    path: GLOBAL_ROUTES.FEATURE, loadChildren: () => import('./features/features.module').then((m) => m.FeaturesModule)
  },
  {
    path: GLOBAL_ROUTES.HOME, loadChildren: () => import('./views/home/home.module').then((m) => m.HomeModule)
  },
  {
    path: GLOBAL_ROUTES.NOTFOUND, loadChildren: () => import('./views/not-found/not-found.module').then((m) => m.NotFoundModule)
  },
  {
    path: '**', redirectTo: GLOBAL_ROUTES.NOTFOUND
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
