import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { map } from "rxjs/operators";
import { Observable, Subscription } from "rxjs";
import { AppState, userSelector, updateUser } from 'src/app/state';
import { User } from 'src/app/shared/model/user.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  
}
