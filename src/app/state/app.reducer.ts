

import { createReducer, on, Action } from "@ngrx/store";
import { User } from '../shared/model/user.interface';
import { updateUser } from './app.action';

export interface AppState {
    user: User
}

const initialState: AppState = {
    user: { firstName: 'Rashid', lastName: 'Aslam', isAuthorized:true }
}

const reducer = createReducer(initialState,
    on(updateUser, (state, { user }) => {
        return { ...state, user }
    })
)


export function appReducer(state: AppState | undefined, action: Action) {
    return reducer(state, action)
}