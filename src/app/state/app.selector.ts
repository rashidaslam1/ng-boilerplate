import { createSelector, createFeatureSelector } from "@ngrx/store";
import { AppState } from './app.reducer';
import { User } from '../shared/model/user.interface';


export const getAppState = createFeatureSelector<AppState>('appState');

export const userSelector = createSelector(
  getAppState,
  (state: AppState) => {
      return state.user;
  }
);
