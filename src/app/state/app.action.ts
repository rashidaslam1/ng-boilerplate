import { createAction, props } from "@ngrx/store";
import { User } from '../shared/model/user.interface';


export const updateUser = createAction('[User Action] Update User',
    props<{user:User}>());


